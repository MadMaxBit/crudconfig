angular.module('PettyCashApp', 
    ['angular-growl',
    'ngAnimate',
    'ngMessages'])
    .config(function (growlProvider) {
        growlProvider.globalTimeToLive({success: 1000, error: 2000,
            warning: 3000, info: 4000});
    })
    .constant('CONFIGURACIONURLS' , {
        //local : "192.168.10.112"
        local : "http://localhost:8080"
    });