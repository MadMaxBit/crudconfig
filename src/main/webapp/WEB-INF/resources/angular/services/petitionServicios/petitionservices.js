angular.module('PettyCashApp')
        .factory('PetitionService', ['$http', '$q', 'CONFIGURACIONURLS', function ($http, $q ,CONFIGURACIONURLS) {
                return {
                    createPetition: function (petition) {
                        return $http.post(CONFIGURACIONURLS.local+'/WebProjectJava/petition/', petition)
                            .then(
                                    function (response) {
                                        return response.data;
                                    },
                                    function (errResponse) {
                                        return $q.reject(errResponse);
                                    }
                            );
                    }
                };
            }]);