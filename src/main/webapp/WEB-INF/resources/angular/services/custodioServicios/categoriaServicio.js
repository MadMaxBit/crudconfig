angular.module('SistemaActividades')
        .factory('CategoriaService', ['$http', '$q', 'CONFIGURACIONURLS', function ($http, $q, CONFIGURACIONURLS) {
                return {
                    getAllCategoria: function () {
                        return $http.get('http://' + CONFIGURACIONURLS.local + ':8080/AdministradorActividades/categoria/')
                                .then(
                                        function (response) {
                                            console.log('Inside Categoria-Servicio consultarCategoria' + response.data);
                                            return response.data;
                                        },
                                        function (errResponse) {
                                            console.log('Error Inside Categoria-Servicio consultarCategoria');
                                            console.log('el error tiene : ' + errResponse);
                                            return $q.reject(errResponse);
                                        }
                                );
                    },
                    getCategoriasTiposDistinct: function () {
                        return $http.get('http://' + CONFIGURACIONURLS.local + ':8080/AdministradorActividades/categoria/tiposDistinct')
                                .then(
                                        function (response) {
                                            console.log('Inside Categoria-Servicio  getCategoriasTiposDistinct' + response.data);
                                            return response.data;
                                        },
                                        function (errResponse) {
                                            console.log('Error Inside Categoria-Servicio  getCategoriasTiposDistinct');
                                            console.log('el error tiene : ' + errResponse);
                                            return $q.reject(errResponse);
                                        }
                                );
                    },
                    getCategoriasTiposDistinctParaActividades: function () {
                        return $http.get('http://' + CONFIGURACIONURLS.local + ':8080/AdministradorActividades/categoria/getCategoriasTiposDistinctParaActividades')
                                .then(
                                        function (response) {
                                            console.log('Inside Categoria-Servicio  getCategoriasTiposDistinctParaActividades' + response.data);
                                            return response.data;
                                        },
                                        function (errResponse) {
                                            console.log('Error Inside Categoria-Servicio  getCategoriasTiposDistinctParaActividades');
                                            console.log('el error tiene : ' + errResponse);
                                            return $q.reject(errResponse);
                                        }
                                );
                    },
                    getCategoriasSepDistinct: function () {
                        return $http.get('http://' + CONFIGURACIONURLS.local + ':8080/AdministradorActividades/categoria/tiposSepDistinct')
                                .then(
                                        function (response) {
                                            console.log('Inside Categoria-Servicio  getCategoriasSepDistinct' + response.data);
                                            return response.data;
                                        },
                                        function (errResponse) {
                                            console.log('Error Inside Categoria-Servicio  getCategoriasSepDistinct');
                                            console.log('el error tiene : ' + errResponse);
                                            return $q.reject(errResponse);
                                        }
                                );
                    },
                    getSubCategoriasPorTipo: function (tipo) {
                        return $http.get('http://' + CONFIGURACIONURLS.local + ':8080/AdministradorActividades/categoria/subCategoriasTipo/' + tipo)
                                .then(
                                        function (response) {
                                            console.log('Inside Categoria-Servicio  getSubCategoriasPorTipo' + response.data);
                                            return response.data;
                                        },
                                        function (errResponse) {
                                            console.log('Error Inside Categoria-Servicio  getSubCategoriasPorTipo');
                                            console.log('el error tiene : ' + errResponse);
                                            return $q.reject(errResponse);
                                        }
                                );
                    },
                    getSubCategoriasPorTipoActivas: function (tipo) {
                        return $http.get('http://' + CONFIGURACIONURLS.local + ':8080/AdministradorActividades/categoria/subCategoriasTipoActivas/' + tipo)
                                .then(
                                        function (response) {
                                            console.log('Inside Categoria-Servicio  getSubCategoriasPorTipo' + response.data);
                                            return response.data;
                                        },
                                        function (errResponse) {
                                            console.log('Error Inside Categoria-Servicio  getSubCategoriasPorTipo');
                                            console.log('el error tiene : ' + errResponse);
                                            return $q.reject(errResponse);
                                        }
                                );
                    },
                    getCategoriasPorId: function (id) {
                        return $http.get('http://' + CONFIGURACIONURLS.local + ':8080/AdministradorActividades/categoria/getCategoriasPorId/' + id)
                                .then(
                                        function (response) {
                                            console.log('Inside Categoria-Servicio  getCategoriasPorId' + response.data);
                                            return response.data;
                                        },
                                        function (errResponse) {
                                            console.log('Error Inside Categoria-Servicio  getCategoriasPorId');
                                            console.log('el error tiene : ' + errResponse);
                                            return $q.reject(errResponse);
                                        }
                                );
                    },
                    getEstatusDocPorRol: function () {
                        return $http.get('http://' + CONFIGURACIONURLS.local + ':8080/AdministradorActividades/categoria/llenarSelectEstatusDocPorRol/')
                                .then(
                                        function (response) {
                                            console.log('Inside Categoria-Servicio  getEstatusDocPorRol' + response.data);
                                            return response.data;
                                        },
                                        function (errResponse) {
                                            console.log('Error Inside Categoria-Servicio  getEstatusDocPorRol');
                                            console.log('el error tiene : ' + errResponse);
                                            return $q.reject(errResponse);
                                        }
                                );
                    },
                    createCategoria: function (categoria) {
                        return $http.post('http://' + CONFIGURACIONURLS.local + ':8080/AdministradorActividades/categoria/', categoria)
                                .then(
                                        function (response) {
                                            console.log('Inside Categoria-Servicio crearCategoria');

                                            return response.data;
                                        },
                                        function (errResponse) {
                                            console.error('Error ' + errResponse);
                                            return $q.reject(errResponse);
                                        }
                                );
                    },
                    deleteCategoria: function (id) {
                        return $http.delete('http://' + CONFIGURACIONURLS.local + ':8080/AdministradorActividades/categoria/' + id)
                                .then(
                                        function (response) {
                                            console.log('Inside categoria service deleteCategoria');

                                            return response.data;
                                        },
                                        function (errResponse) {
                                            console.error('Error ' + errResponse);
                                            return $q.reject(errResponse);
                                        }
                                );
                    },
                    editCategoria: function (id, categoria) {
                        return $http.put('http://' + CONFIGURACIONURLS.local + ':8080/AdministradorActividades/categoria/' + id, categoria)
                                .then(
                                        function (response) {
                                            console.log('Inside editCategoria service');

                                            return response.data;
                                        },
                                        function (errResponse) {
                                            console.error('Error ' + errResponse);
                                            return $q.reject(errResponse);
                                        }
                                );
                    },
                    getCategoriasSEPCheckBoxesEditarSEPModal: function (idSep) {
                        return $http.get('http://'
                                + CONFIGURACIONURLS.local
                                + ':8080/AdministradorActividades/sep/editarSepModalCheckBoxes/' + idSep)
                                .then(
                                        function (response) {
                                            console.log('Inside getCategoriasSEPCheckBoxesEditarSEPModal service');

                                            return response.data;
                                        },
                                        function (errResponse) {
                                            console.error('Error en getCategoriasSEPCheckBoxesEditarSEPModal ' + errResponse);
                                            return $q.reject(errResponse);
                                        }
                                );
                    },
                    getCategoriasProyectoCheckBoxesEditarProyecto: function (idProyecto) {
                        return $http.get('http://'
                                + CONFIGURACIONURLS.local
                                + ':8080/AdministradorActividades/proyecto/editarProyectoCheckBoxes/' + idProyecto)
                                .then(
                                        function (response) {
                                            console.log('Inside getCategoriasProyectoCheckBoxesEditarProyecto service');

                                            return response.data;
                                        },
                                        function (errResponse) {
                                            console.error('Error en getCategoriasProyectoCheckBoxesEditarProyecto ' + errResponse);
                                            return $q.reject(errResponse);
                                        }
                                );
                    }
                };
            }]);

