angular.module('PettyCashApp')
        .factory('CustodioService', ['$http', '$q', 'CONFIGURACIONURLS', function ($http, $q, CONFIGURACIONURLS) {
                return {
                    getMoneyBalance: function () {
                        return $http.get(CONFIGURACIONURLS.local + '/WebProjectJava/getMoneyBalance/')
                                .then(
                                        function (response) {
                                            return response.data;
                                        },
                                        function (errResponse) {
                                            return $q.reject(errResponse);
                                        }
                                );
                    },
                    registerMoneyInBank: function (amountToInsert) {
                        return $http.post(CONFIGURACIONURLS.local + '/WebProjectJava/registerMoneyInBank/', amountToInsert)
                                .then(
                                        function (response) {
                                            return response.data;
                                        },
                                        function (errResponse) {
                                            return $q.reject(errResponse);
                                        }
                                );
                    },
                    approvePetition: function (id, petition) {
                        return $http.put(CONFIGURACIONURLS.local + '/WebProjectJava/approvePetition/' + id, petition)
                                .then(
                                        function (response) {
                                            return response.data;
                                        },
                                        function (errResponse) {
                                            console.error('Error in approvePetition' + errResponse);
                                            return $q.reject(errResponse);
                                        }
                                );
                    },
                    getPendingPetitions: function () {
                        return $http.get(CONFIGURACIONURLS.local + '/WebProjectJava/getPendingPetitions/')
                                .then(
                                        function (response) {
                                            return response.data;
                                        },
                                        function (errResponse) {
                                            console.error('Error in getPendingPetitions' + errResponse);
                                            return $q.reject(errResponse);
                                        }
                                );
                    },
                    getApprovedPetitions: function () {
                        return $http.get(CONFIGURACIONURLS.local + '/WebProjectJava/getApprovedPetitions/')
                                .then(
                                        function (response) {
                                            return response.data;
                                        },
                                        function (errResponse) {
                                            console.error('Error in getApprovedPetitions' + errResponse);
                                            return $q.reject(errResponse);
                                        }
                                );
                    }
                };
            }]);