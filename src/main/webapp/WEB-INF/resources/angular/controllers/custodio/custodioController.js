angular.module('PettyCashApp')
        .controller('CustodioCtrl', ['$scope', 'CustodioService',
            function ($scope, CustodioService) {

                var self = this;

                $scope.moneyAmount = {};
                $scope.moneyToInsert = {};


                //REGISTER MONEY IN BANK
                self.submitRegisterMoneyInBank = function (moneyToInsert, isValid) {
                    $scope.submitted = true;
                    if (isValid) {
                        $scope.submitted = false;
                        CustodioService.registerMoneyInBank(moneyToInsert.amount).then(
                                function (response) {
                                    $scope.moneyAmount = {};
                                    $scope.moneyToInsert = {};
                                    alert("Money inserted successfully");
                                    self.getMoneyBalance();
                                },
                                function (errResponse) {
                                    alert(errResponse.data);
                                }
                        );
                    }
                };

                self.getMoneyBalance = function () {
                    CustodioService.getMoneyBalance()
                            .then(
                                    function (d) {
                                        $scope.moneyAmount = d;
                                    },
                                    function (errResponse) {
                                        console.error('error inside controller getMoneyBalance ', errResponse);
                                    }
                            );
                };
                
                self.getMoneyBalance();
            }
        ]);