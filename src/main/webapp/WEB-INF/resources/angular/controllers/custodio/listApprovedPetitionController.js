angular.module('PettyCashApp')
        .controller('ListApprovedPetitionCtrl', ['$scope', 'CustodioService',
            function ($scope, CustodioService) {

                var self = this;

                $scope.moneyAmount = {};
                $scope.listOfApprovedPetitions = {};

                self.getApprovedPetitions = function () {
                    CustodioService.getApprovedPetitions()
                            .then(
                                    function (d) {
                                        $scope.listOfApprovedPetitions = d;
                                    },
                                    function (errResponse) {
                                        console.error('error inside controller getApprovedPetitions ', errResponse);
                                    }
                            );
                };
                self.getApprovedPetitions();

                self.getMoneyBalance = function () {
                    CustodioService.getMoneyBalance()
                            .then(
                                    function (d) {
                                        $scope.moneyAmount = d;
                                    },
                                    function (errResponse) {
                                        console.error('error inside controller getMoneyBalance ', errResponse);
                                    }
                            );
                };
                self.getMoneyBalance();
            }
        ]);