angular.module('PettyCashApp')
        .controller('ApprovePetitionCtrl', ['$scope', 'CustodioService',
            function ($scope, CustodioService) {

                var self = this;

                $scope.moneyAmount = {};
                $scope.listOfPendingPetitions = {};

                //approve petition
                self.approvePetition = function (petition) {
                    CustodioService.approvePetition(petition.idPetition, petition).then(
                            function (response) {
                                $scope.moneyAmount = {};
                                $scope.moneyToInsert = {};
                                alert("Petition aprroved successfully");
                                self.getMoneyBalance();
                                self.getPendingPetitions();
                            },
                            function (errResponse) {
                                alert(errResponse.data);
                            }
                    );
                };

                self.getPendingPetitions = function () {
                    CustodioService.getPendingPetitions()
                            .then(
                                    function (d) {
                                        $scope.listOfPendingPetitions = d;
                                    },
                                    function (errResponse) {
                                        console.error('error inside controller getPendingPetitions ', errResponse);
                                    }
                            );
                };
                self.getPendingPetitions();

                self.getMoneyBalance = function () {
                    CustodioService.getMoneyBalance()
                            .then(
                                    function (d) {
                                        console.log("D VALE ", d);
                                        $scope.moneyAmount = d;
                                    },
                                    function (errResponse) {
                                        console.error('error inside controller getMoneyBalance ', errResponse);
                                    }
                            );
                };
                self.getMoneyBalance();
            }
        ]);