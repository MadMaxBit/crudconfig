angular.module('SistemaActividades')
        .controller('CategoriaCtrl', ['$scope', 'CategoriaService', 'modalService',
            'CrearVentanasModalSerivce', 'growl',
            function ($scope, CategoriaService, modalService,
                    CrearVentanasModalSerivce, growl) {

                var self = this;
                $scope.categoria = {};

                $scope.tiposEstado = [
                    {id: 1, estado: 'Activo', descripcion: 'a'},
                    {id: 2, estado: 'Inactivo', descripcion: 'b'}
                ];

                //**************************************************************
                //**************                                     ***********
                //**************       LLENAR SELECTS                ***********
                //**************                                     ***********
                //**************************************************************

                //SELECT MARTECSOL
                //LLENA UN SELECT CON TODAS LOS TIPOS DE LAS MARCAS TECNOLOGIAS
                self.getAllCategoria = function () {
                    CategoriaService.getAllCategoria()
                            .then(
                                    function (d) {
                                        $scope.listaCategoria = d;
                                    },
                                    function (errResponse) {
                                        console.error('inside controller getAllCategoria');
                                    }
                            );
                };
                //SELECT MARTECSOL
                //LLENA UN SELECT CON TODAS DIFERENTES TIPOS DE CATEGORIAS Sin QUE SE REPITAN
                self.getCategoriasTiposDistinct = function () {
                    CategoriaService.getCategoriasTiposDistinct()
                            .then(
                                    function (d) {
                                        //console.log('DDDD VALE ' + JSON.stringify(d, null , 2));
                                        //NUEVO CAMBIAR PRIMERA LETRA
                                        var newList = self.cambiarPrimeraLetraTipoCat(d);
                                        console.log('newList vale ***' + JSON.stringify(newList));

//                                        $scope.listaCategoria = d;
                                        $scope.listaCategoria = newList;
                                    },
                                    function (errResponse) {
                                        console.error('inside controller getCategoriasTiposDistinct');
                                    }
                            );
                };

                self.cambiarPrimeraLetraTipoCat = function (listaCat) {
                    var nuevaLista = [];
                    for (var i = 0; i < listaCat.length; i++) {
                        if (listaCat[i].tipo !== 'estatus documento') {
                            listaCat[i].tipo
                                    = capitalizeFirstLetter(listaCat[i].tipo);
                            nuevaLista.push(listaCat[i]);
                        }
                    }
                    return nuevaLista;
                };
                function capitalizeFirstLetter(string) {
//                    return string.charAt(0).toUpperCase() + string.slice(1);
                    return string.replace(/\w\S*/g, function (txt) {
                        return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
                    });
                }

                //SELECT SUB CATEGORIAS
                //LLENA UN SELECT CON TODAS LAS SUBCATEGORIAS QUE TIENE UNA
                //CATEGORIA
                self.getSubCategoriasPorTipo = function (tipo) {
                    CategoriaService.getSubCategoriasPorTipo(tipo)
                            .then(
                                    function (d) {
                                        console.log(' la nueva data traida vale ' + JSON.stringify(d));
                                        $scope.listaItemsMismaCaegoria = d;
                                    },
                                    function (errResponse) {
                                        console.error('inside controller getSubCategoriasPorTipo');
                                    }
                            );
                };
//                self.getAllCategoria();
                self.getCategoriasTiposDistinct();
                //Select CategoriaTipo
                //CARGA LOS NOMBRES DE UNAS CATEGORIAS SEGUN SU TIPO 
                $scope.selectTipoCategoriaTraerCategoriasMismoTipo = function (tipo) {

                    if ($scope.selectCategoria != null) {
                        $scope.selectCategoria = null;
                    }
                    console.log('tipo valavlvla ' + tipo);
                    console.log('tipo valavlvla lower' + String(tipo).toLowerCase());
//                    $scope.categoria.nombre = "";
//                    $scope.categoria.descripcion = "";
                    $scope.categoria = {};
                    self.getSubCategoriasPorTipo(String(tipo).toLowerCase());
                    //METODO VIEJO DE TRAERSE SUB CATEGORIAS CUANDO SE TENIA EL PRIMER
                    //SELECT LLENO CON TODOS LOS DATOS REPETIDOS
//                    var tipoCategoria = $scope.selectCategoriaTipo.tipo;
//
//                    console.log('la variable tipo vale ' + tipoCategoria);
//                    var listaItemsCategoriaPorTipo = [];
//                    for (var categoria  in $scope.listaCategoria) {
//                        if (tipoCategoria === $scope.listaCategoria[categoria].tipo) {
//                            listaItemsCategoriaPorTipo.push($scope.listaCategoria[categoria]);
//                        }
//                    }

//                    $scope.listaItemsMismaCaegoria = listaItemsCategoriaPorTipo;
                };
                //SELECT CATEGORIA 
                //TRAE LOS DATOS DE UNA CATEGORIA
                $scope.selectCategoriaTraerDatos = function () {

                    if ($scope.selectCategoria != null)
                    {
                        $scope.categoria.nombre = $scope.selectCategoria.nombre;
                        $scope.categoria.descripcion = $scope.selectCategoria.descripcion;
                        $scope.categoria.nombreSubCat = $scope.selectCategoria.nombreSubCat;
                        $scope.categoria.descripcionSubCat = $scope.selectCategoria.descripcionSubCat;

                        console.log('$scope.selectCategoria.estado: vale ' + $scope.selectCategoria.estado);
                        if ($scope.selectCategoria.estado != null) {
                            $scope.categoria.selectEstadoTipo = {"estado": $scope.selectCategoria.estado};
                        } else {
                            $scope.categoria.selectEstadoTipo = {"estado": "Activo"};
                        }
                    }

                };
                //**************************************************************
                //**************                                     ***********
                //**************       CREAR CATEGORIA               ***********
                //**************                                     ***********
                //**************************************************************

                //---------------------CREAR CATEGORIA--------------------------
                self.createCategoria = function (categoria) {

                    var tituloConfirmacion = 'Crear Categoria';
                    var msjConfirmacion = 'Esta seguro que desea crear la siguiente Categoria';
                    var tituloExito = 'Categoria Creada';
                    var msjExito = 'Se creo la Categoria llamada ' + categoria.nombre;
                    var tituloError = 'ERROR al crear Categoria';
                    var msjError = 'ERROR: NO se pudo crear la Categoria ' + categoria.nombre;
                    var ventanasModal = CrearVentanasModalSerivce.crearTodasLasVentanasModal(tituloConfirmacion,
                            msjConfirmacion, tituloExito, msjExito, tituloError, msjError);
                    modalService.showModal({}, ventanasModal.ventanaConfirmacion).then(function (result) {
                        CategoriaService.createCategoria(categoria)
                                .then(
                                        function (response) {

                                            growl.success("Se creo la categoria " + categoria.nombre
                                                    + " desea consultarla: " +
                                                    "<a href='/../AdministradorActividades/categoria/consultarCategoria' class='btn btn-info'> " +
                                                    "<i class='fa fa-search'></i>" +
                                                    " Consultar Categorias" +
                                                    "</a>",
                                                    {title: "Categoria Creada",
                                                        ttl: 9500,
                                                        reversedOrder: true,
                                                        disableCountDown: true,
                                                        onlyUniqueMessages: false
                                                    });

                                            $scope.categoria = null;
                                            $scope.selectCategoria = null;

                                        },
                                        function (errResponse) {
                                            var modalConfirmacionError = {
                                                actionButtonText: 'OK',
                                                headerText: 'Crear Categoria',
                                                bodyText: 'ERROR: No se pudo crear la categoria ' + categoria.nombre
                                                        + ' debido a que: ' + errResponse.data
                                            };
                                            modalService.showModal(ventanasModal.ventanaDefault, modalConfirmacionError);
                                            console.error('Inside CREAR Categoria controller Error while creating categoria.');
                                        }
                                );
                    });
                };
                self.submitcreateCategoria = function (categoria, isValid) {

                    console.log('hice click');
                    $scope.submitted = true;
                    if (isValid) {
                        $scope.submitted = false;
                        categoria.idCategoria = '0';
                        categoria.tipo = String($scope.selectCategoria.tipo).toLocaleLowerCase();

                        console.log('antes de mandar categoria vale ' + JSON.stringify(categoria, null, 2));
                        self.createCategoria(categoria);
                    }
                };
                //**************************************************************
                //**************                                     ***********
                //**************   ELIMNAR CATEGORIA                 ***********
                //**************                                     ***********
                //**************************************************************

                //---------------------ELIMINAR CATEGORIA-----------------------
                self.deleteCategoria = function (categoria) {

                    var tituloConfirmacion = 'Eliminar Categoria';
                    var msjConfirmacion = 'Esta seguro que desea eliminar la Categoria?';
                    var tituloExito = 'Categoria Eliminada';
                    var msjExito = 'Se elimino la Caegoria llamada ' + categoria.nombre;
                    var tituloError = 'ERROR al eliminar Categoria';
                    var msjError = 'ERROR: NO se pudo eliminar la Categoria ' + categoria.nombre;
                    var ventanasModal = CrearVentanasModalSerivce.crearTodasLasVentanasModal(tituloConfirmacion,
                            msjConfirmacion, tituloExito, msjExito, tituloError, msjError);
                    modalService.showModal({}, ventanasModal.ventanaConfirmacion).then(function (result) {
                        CategoriaService.deleteCategoria(categoria.idCategoria)
                                .then(
                                        function (response) {

                                            CategoriaService.getCategoriasTiposDistinct()
                                                    .then(
                                                            function (d) {
                                                                var newList = self.cambiarPrimeraLetraTipoCat(d);
//                                                                $scope.listaCategoria = d;
                                                                $scope.listaCategoria = newList;
                                                                $scope.selectCategoriaTipo = null;
                                                                growl.success('Se elimino la categoria ' + categoria.nombre,
                                                                        {title: 'Categoria Eliminada',
                                                                            ttl: 2500,
                                                                            disableCountDown: true,
                                                                            onlyUniqueMessages: false,
                                                                            globalReversedOrder: true});


                                                                $scope.listaItemsMismaCaegoria = null;
                                                                $scope.categoria = {};
                                                            },
                                                            function (errResponse) {
                                                                console.error('inside controller getCategoriasTiposDistinct');
                                                            }
                                                    );

                                        },
                                        function (errResponse) {
                                            modalService.showModal(ventanasModal.ventanaDefault, ventanasModal.ventanaError);
                                            console.error('Inside EliminarCategoria controller Error');
                                        }
                                );
                    });
                };
                self.submitdeleteCategoria = function (isValid) {
                    console.log('hice click');
                    $scope.submitted = true;
                    if (isValid) {
                        $scope.submitted = false;
                        var categoria = {};
                        categoria.nombre = $scope.selectCategoria.nombre;
                        categoria.idCategoria = $scope.selectCategoria.idCategoria;
                        self.deleteCategoria(categoria);
                    }
                };
                //**************************************************************
                //**************                                     ***********
                //**************   EDITAR CATEGORIA                  ***********
                //**************                                     ***********
                //**************************************************************

                //---------------------EDITAR MAR TEC SOL--------------------
                self.editCategoria = function (categoria) {

                    var tituloConfirmacion = 'Editar Categoria';
                    var msjConfirmacion = 'Esta seguro que desea editar la Categoria?';
                    var tituloExito = 'Categoria';
                    var msjExito = 'Se edito la Categoria llamada ' + categoria.nombre;
                    var tituloError = 'ERROR al editar Categoria';
                    var msjError = 'ERROR: NO se pudo editar la Categoria ' + categoria.nombre;
                    var ventanasModal = CrearVentanasModalSerivce.crearTodasLasVentanasModal(tituloConfirmacion,
                            msjConfirmacion, tituloExito, msjExito, tituloError, msjError);
                    modalService.showModal({}, ventanasModal.ventanaConfirmacion).then(function (result) {
                        CategoriaService.editCategoria(categoria.idCategoria, categoria)
                                .then(
                                        function (response) {
                                            //PRUEBA GROWL 2
//                                            growl.success("<b>I'm</b> a success message", configuracionGrowl);

                                            growl.success('Se edito la categoria ' + categoria.nombre,
                                                    {title: 'Categoria Editada',
                                                        ttl: 2500,
                                                        disableCountDown: true,
                                                        onlyUniqueMessages: false,
                                                        globalReversedOrder: true});

                                            $scope.selectCategoria = null;
                                            $scope.selectCategoriaTipo = null;
                                            $scope.categoria = {};



//                                              growl.success('Se edito la categoria', config);
//                                            modalService.showModal(ventanasModal.ventanaDefault, ventanasModal.ventanaExito);
                                        },
                                        function (errResponse) {
                                            var modalConfirmacionError = {
                                                actionButtonText: 'OK',
                                                headerText: 'Editar Categoria',
                                                bodyText: 'ERROR: No se pudo editar la categoria ' + categoria.nombre
                                                        + ' debido a que: ' + errResponse.data
                                            };
//                                            modalService.showModal(ventanasModal.ventanaDefault, modalConfirmacionError);

                                            growl.error('No se puso editar edito la categoria debido a que: '
                                                    + errResponse.data,
                                                    {title: 'Error', ttl: 5500,
                                                        disableCountDown: true,
                                                        onlyUniqueMessages: false});
                                            console.error('Inside EDITAR CATEGORIA controller Error');
                                        }
                                );
                    });
                };
                self.submitEditCategoria = function (categoria, isValid) {
                    console.log('hice click');
                    $scope.submitted = true;
                    if (isValid) {
                        $scope.submitted = false;
                        categoria.idCategoria = $scope.selectCategoria.idCategoria;
                        categoria.tipo = $scope.selectCategoria.tipo;
                        categoria.estado = $scope.categoria.selectEstadoTipo.estado;
                        console.log('antes de mandar categoria tiene ' + JSON.stringify(categoria, null, 2));
                        self.editCategoria(categoria);
                    }
                };
//                PRUEBA NOTIFICACIONES GROWL 2



            }]);

