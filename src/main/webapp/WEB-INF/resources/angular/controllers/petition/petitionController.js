angular.module('PettyCashApp')
        .controller('PetitionCtrl', ['$scope', 'PetitionService', 'growl',
            function ($scope, PetitionService, growl) {

                var self = this;

                $scope.petition = {};
                

                self.submitCreatePetition = function (petition, isValid) {

                    $scope.submitted = true;
                    if (isValid) {
                        $scope.submitted = false;
                        PetitionService.createPetition(petition).then(
                                function (response) {
                                    $scope.petition = {};
                                    alert("Petition successfully created")
                                },
                                function (errResponse) {
                                    alert(errResponse.data);
                                }
                        );
                    }
                };
            }
        ]);