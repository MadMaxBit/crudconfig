/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exceptions;

/**
 *
 * @author SupraBT
 */
public class CustomException extends Exception {

    private String codeError;
    private String messageError;

    public String getCodeError() {
        return codeError;
    }

    public void setCodeError(String codeError) {
        this.codeError = codeError;
    }

    public String getMessageError() {
        return messageError;
    }

    public void setMessageError(String messageError) {
        this.messageError = messageError;
    }

    public CustomException(String codeError, String messageError) {
        this.codeError = codeError;
        this.messageError = messageError;
    }

    public CustomException(String msg) {
        super(msg);
    }

}
