package model;

import java.math.BigDecimal;
import java.sql.Timestamp;

public class CashPetition {
    
    private Integer idPetition;
    private String name;
    private BigDecimal amount;
    private Timestamp creationDate;
    private String status ; 
    private Timestamp approvedRejectedDate;
    
    

    public CashPetition() {}

    public CashPetition(Integer idPetition, String name, BigDecimal amount, Timestamp creationDate, String status, Timestamp approvedRejectedDate) {
        this.idPetition = idPetition;
        this.name = name;
        this.amount = amount;
        this.creationDate = creationDate;
        this.status = status;
        this.approvedRejectedDate = approvedRejectedDate;
    }

    public Integer getIdPetition() {
        return idPetition;
    }

    public void setIdPetition(Integer idPetition) {
        this.idPetition = idPetition;
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Timestamp getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Timestamp creationDate) {
        this.creationDate = creationDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Timestamp getApprovedRejectedDate() {
        return approvedRejectedDate;
    }

    public void setApprovedRejectedDate(Timestamp approvedRejectedDate) {
        this.approvedRejectedDate = approvedRejectedDate;
    }
}
