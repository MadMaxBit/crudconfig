package dao;

import exceptions.CustomException;
import java.math.BigDecimal;
import model.CashPetition;


public interface CustodioBankDAO {
    
    /**
     * gets the current cash amount that's stored in the table money_bank from the
     * database
     * @return a BigDecimal with the current cash amount
     */
    public BigDecimal getCurrentCashAmout();
    
    /**
     * Updates the amount column in the table money_bank in the database with
     * the type deposit
     * @param amount 
     */
    public void registeringMoneyInBank(BigDecimal amount);
    
    /**
     * Updates the amount column in the table money_bank in the database with
     * the type withdraw
     * @param amount 
     * @throws exceptions.CustomException 
     */
    public void withdrawMoneyInBank(BigDecimal amount) throws CustomException;
    
    /**
     * Checks if the petition amount is bigger than the current amount of money
     * @param petitionAmount
     * @return a boolean true if it's greater and false if it's not
     */
    public boolean isPetitionGreaterThanCurrentCash(BigDecimal petitionAmount);
    
    /**
     * Updates the amount column in the table money_bank in the database subtracting
     * the petition amount
     * @param petition to approve
     * @throws exceptions.CustomException 
     */
    public void approvePetition(CashPetition petition) throws CustomException;;
    
    
    
    
    
        
}
