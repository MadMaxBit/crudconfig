package dao;

import exceptions.CustomException;
import java.util.ArrayList;
import model.CashPetition;
import model.Person;

public interface CashPetitionDAO {
    
    /**
     * Inserts a cash petition record in the table Petition in the Database when
     * the user press the accept button in the page cratePetition.html
     * @param petition 
     * @throws exceptions.CustomException 
     */
    public void insertPetition(CashPetition petition) throws CustomException;
    
    
    /**
     * Gets an array of all the records in the petition table in the database
     * @return an arrayList of CashPetition objects
     */
    public ArrayList<CashPetition> getAllPetitions();
    
    /**
     * Gets an array of all the records in the petition table in the database that
     * have a status passed as a parameter
     * @param status
     * @return an arrayList of CashPetition objects
     */
    public ArrayList<CashPetition> getAllPetitionsByStatus(String status);
    
    
    
    public ArrayList<Person> testMysql();
}
