package config;

import dao.CashPetitionDAO;
import dao.CustodioBankDAO;
import daoImplementation.CashPetitionImplementation;
import daoImplementation.CustodioImplementation;
import javax.sql.DataSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.freemarker.FreeMarkerConfigurer;
import org.springframework.web.servlet.view.freemarker.FreeMarkerViewResolver;

@Configuration
@EnableWebMvc
@ComponentScan({"config", "controllers", "security"})
public class ConfigMVC extends WebMvcConfigurerAdapter {

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/resources/**").addResourceLocations("/WEB-INF/resources/");

    }

    //BEANS
    @Bean
    public CashPetitionDAO getCashPetitionDAO() {
        return new CashPetitionImplementation(getDataSource());
    }
    
    @Bean
    public CustodioBankDAO getCustodioBankDAO() {
        return new CustodioImplementation(getDataSource());
    }
    

    //JDBCTEMPLATE POSTGRESQL
//    @Bean
//    public DataSource getDataSource() {
//        DriverManagerDataSource dataSource = new DriverManagerDataSource();
//        dataSource.setDriverClassName("org.postgresql.Driver");
//        dataSource.setUrl("jdbc:postgresql://localhost/pettycashdb");
//        dataSource.setUsername("postgres");
//        dataSource.setPassword("1234");
//
//        return dataSource;
//    }
    
    //JDBCTEMPLATE MYSQL
    @Bean
    public DataSource getDataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName("com.mysql.jdbc.Driver");
        dataSource.setUrl("jdbc:mysql://localhost:3306/rejava");
        dataSource.setUsername("rejava");
        dataSource.setPassword("123");

        return dataSource;
    }
    
    /**
     * The View resolver to use when resolving FreeMarker views.
     *
     * @return the View Resolver Object used to resolve FreeMarker views.
     */
    @Bean
    public FreeMarkerConfigurer freemarkerConfig() {
        FreeMarkerConfigurer freeMarkerConfigurer = new FreeMarkerConfigurer();
        freeMarkerConfigurer.setTemplateLoaderPath("/WEB-INF/views/");
        freeMarkerConfigurer.setDefaultEncoding("UTF-8");
        return freeMarkerConfigurer;
    }

    @Bean
    public FreeMarkerViewResolver viewResolver() {

        FreeMarkerViewResolver viewResolver = new FreeMarkerViewResolver();
        viewResolver.setPrefix("");
        viewResolver.setSuffix(".html");
        viewResolver.setContentType("text/html;charset=UTF-8");
        viewResolver.setCache(false);   

        return viewResolver;
    }

}
