package config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import security.SecurityConfig;


@Configuration
@ComponentScan(basePackages = {"controllers", "security"})
@Import({ConfigMVC.class, SecurityConfig.class})
public class RootConfig {
    
}
