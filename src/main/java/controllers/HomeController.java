package controllers;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class HomeController {

    private static final Logger logger = Logger.getLogger(HomeController.class);
  

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String index(ModelMap map) {

        map.put("msg", "Hello Spring 4 Web MVC!");

        return "index";
    }
    
    //este mapping es para retornar la pagina personalizada de la execepcion 404
    @RequestMapping(value = "/paginaNoMapeadaUno", method = RequestMethod.GET)
    public String paginaSinMap(ModelMap map) {

        logger.info("This is an info log entry");
        logger.error("This is an error log entry");

        return "paginaNoMapeadaUno";
    }

}
