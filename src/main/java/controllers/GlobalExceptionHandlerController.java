package controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import exceptions.CustomException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.NoHandlerFoundException;

@ControllerAdvice
public class GlobalExceptionHandlerController {

    
    //404 exceptions method
    @ExceptionHandler(NoHandlerFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public String handle(NoHandlerFoundException ex) {

        System.out.println("A 404 Exception ocurred");
        return "paginaNoMapeadaUno";
    }

    @ExceptionHandler(value = {CustomException.class})
    protected ResponseEntity<String> handleExcepcionCustomizada(CustomException ex, WebRequest request) {

        String jsonInString = null;
        try {
            ObjectMapper mapper = new ObjectMapper();
            jsonInString = mapper.writeValueAsString(ex.getMessageError());
            System.out.println(jsonInString);

        } catch (JsonProcessingException ex1) {
            System.out.println("A global custom exception ocurred");
        }
        return new ResponseEntity<String>(jsonInString, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    
}
