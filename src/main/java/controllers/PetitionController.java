package controllers;

import dao.CashPetitionDAO;
import exceptions.CustomException;
import model.CashPetition;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.util.UriComponentsBuilder;

@Controller
public class PetitionController {
    
    @Autowired
    CashPetitionDAO cashPetitionDAO;
    
    @RequestMapping(value = "/addPetition", method = RequestMethod.GET)
    public String publicPagina(ModelMap map) {
        return "addPetition";
    }
    
    //-------------------Create a Petition--------------------------------------
    @RequestMapping(value = "/petition/", method = RequestMethod.POST)
    public ResponseEntity<Void> createPetition(@RequestBody CashPetition petition, UriComponentsBuilder ucBuilder) 
            throws CustomException {

        cashPetitionDAO.insertPetition(petition);

        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucBuilder.path("/petition/{id}").buildAndExpand(petition.getIdPetition()).toUri());

        return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
    }
    
//    @RequestMapping(value = "/categoria/{id}", method = RequestMethod.DELETE)
//    public ResponseEntity<Categoria> deleteCategoria(@PathVariable("id") int id) throws ExcepcionCustomizada {
//        System.out.println("Fetching & Deleting Categoria with id " + id);
//
//        categoriaDAO.eliminarCategoriaPorId(id);
//
//        return new ResponseEntity<Categoria>(HttpStatus.NO_CONTENT);
//    }
//
//    //---------------------UPDATE CATEGORIA ------------------------------------
//    @RequestMapping(value = "/categoria/{id}", method = RequestMethod.PUT)
//    public ResponseEntity<Void> updateCategoria(@PathVariable("id") int id, @RequestBody Categoria categoria, UriComponentsBuilder ucBuilder) throws ExcepcionCustomizada {
//
//        HttpHeaders headers = new HttpHeaders();
//
//        headers.setLocation(ucBuilder.path("/categoria/{id}").buildAndExpand(id).toUri());
//        System.out.println("el tipo vale " + categoria.getTipo());
//        categoriaDAO.modificarCategoria(categoria);
//
//        return new ResponseEntity<Void>(headers, HttpStatus.OK);
//    }
}
