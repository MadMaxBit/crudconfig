package controllers;

import dao.CashPetitionDAO;
import dao.CustodioBankDAO;
import exceptions.CustomException;
import java.math.BigDecimal;
import java.util.List;
import model.CashPetition;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.util.UriComponentsBuilder;

@Controller
public class CustodioController {
    
    @Autowired
    CustodioBankDAO custodioBankDAO;
    
    @Autowired
    CashPetitionDAO cashPetitionDAO;
    
    
    @RequestMapping(value = "/registerMoneyInBank", method = RequestMethod.GET)
    public String registerMoneyInBank(ModelMap map) {

        return "registerMoneyInBank";
    }
    
    @RequestMapping(value = "/approvePetitionCustodio", method = RequestMethod.GET)
    public String approvePetitionCustodio(ModelMap map) {

        return "approvePetitionCustodio";
    }
    

    @RequestMapping(value = "/listApprovedPetitions", method = RequestMethod.GET)
    public String listApprovedPetitionController(ModelMap map) {

        return "listApprovedPetitions";
    }
    
    //-------------------Register money in Bank---------------------------------
    @RequestMapping(value = "/registerMoneyInBank/", method = RequestMethod.POST)
    public ResponseEntity<Void> registerMoneyInBank(@RequestBody BigDecimal amountToInsert, UriComponentsBuilder ucBuilder) {

        custodioBankDAO.registeringMoneyInBank(amountToInsert);

        return new ResponseEntity<Void>(HttpStatus.CREATED);
    }
    
    //-------------------------Get current amount balance in bank---------------
    @RequestMapping(value = "/getMoneyBalance/", method = RequestMethod.GET)
    public ResponseEntity<BigDecimal> getCurrentBalanceInBank() {
        return new ResponseEntity<BigDecimal>(custodioBankDAO.getCurrentCashAmout(), HttpStatus.OK);
    }
    
    //-------------------------Get pendind petitions----------------------------
    @RequestMapping(value = "/getPendingPetitions/", method = RequestMethod.GET)
    public ResponseEntity<List<CashPetition>> getPendingPetitions() {
        return new ResponseEntity<List<CashPetition>>(cashPetitionDAO.getAllPetitionsByStatus("pending"), HttpStatus.OK);
    }
    
    //-------------------------Get approved petitions----------------------------
    @RequestMapping(value = "/getApprovedPetitions/", method = RequestMethod.GET)
    public ResponseEntity<List<CashPetition>> getApprovedPetitions() {
        return new ResponseEntity<List<CashPetition>>(cashPetitionDAO.getAllPetitionsByStatus("approved"), HttpStatus.OK);
    }
    
    //---------------------Approve petition ------------------------------------
    @RequestMapping(value = "/approvePetition/{id}", method = RequestMethod.PUT)
    public ResponseEntity<Void> approvePetition(@PathVariable("id") int id, @RequestBody CashPetition petition, UriComponentsBuilder ucBuilder) 
            throws CustomException {

        HttpHeaders headers = new HttpHeaders();

        headers.setLocation(ucBuilder.path("/approvePetition/{id}").buildAndExpand(id).toUri());
        custodioBankDAO.approvePetition(petition);

        return new ResponseEntity<Void>(headers, HttpStatus.OK);
    }


    
}
