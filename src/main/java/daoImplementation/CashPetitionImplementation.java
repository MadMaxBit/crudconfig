package daoImplementation;

import dao.CashPetitionDAO;
import exceptions.CustomException;
import java.sql.Timestamp;
import java.util.ArrayList;
import javax.sql.DataSource;
import model.CashPetition;
import model.Person;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;

public class CashPetitionImplementation implements CashPetitionDAO {

    private JdbcTemplate jdbcTemplate;

    public CashPetitionImplementation(DataSource datasource) {
        this.jdbcTemplate = new JdbcTemplate(datasource);
    }

    @Override
    public void insertPetition(CashPetition petition) throws CustomException {
        try {

            Timestamp creationDate = new Timestamp(System.currentTimeMillis());

            String sql
                    = "INSERT INTO PETITION (name, amount, creation_date, "
                    + "approved_rejected_date, status) "
                    + "VALUES(?, ?, ?, ?, ?)";

            jdbcTemplate.update(
                    sql,
                    new Object[]{petition.getName(),
                        petition.getAmount(), creationDate,
                        petition.getApprovedRejectedDate(), "pending"});

        } catch (DataAccessException dataAccessException) {
            System.out.println("A DataAccessException ocurred in the method "
                    + "CashPetitionImplementation.insertPettyCash ");
            dataAccessException.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public ArrayList<CashPetition> getAllPetitions() {
        ArrayList<CashPetition> petitionsList = new ArrayList<CashPetition>();

        try {
            String sql
                    = "SELECT id_petition, name, amount, "
                    + "creation_date, approved_rejected_date, status "
                    + "FROM "
                    + "petition";

            petitionsList
                    = (ArrayList<CashPetition>) jdbcTemplate.query(
                            sql,
                            new BeanPropertyRowMapper(CashPetition.class
                            )
                    );

        } catch (DataAccessException dataAccessException) {
            System.out.println("A DataAccessException ocurred in the method "
                    + "CashPetitionImplementation.getAllPetitions ");
            dataAccessException.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return petitionsList;
    }

    @Override
    public ArrayList<CashPetition> getAllPetitionsByStatus(String status) {
        ArrayList<CashPetition> petitionsList = new ArrayList<CashPetition>();

        try {
            String sql
                    = "SELECT id_petition, name, amount, "
                    + "creation_date, approved_rejected_date, status "
                    + "FROM "
                    + "petition "
                    + "WHERE "
                    + "status = ? ";

            petitionsList
                    = (ArrayList<CashPetition>) jdbcTemplate.query(
                            sql,
                            new Object[]{status},
                            new BeanPropertyRowMapper(CashPetition.class)
                    );

        } catch (DataAccessException dataAccessException) {
            System.out.println("A DataAccessException ocurred in the method "
                    + "CashPetitionImplementation.getAllPendingPetitions ");
            dataAccessException.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return petitionsList;
    }

    @Override
    public ArrayList<Person> testMysql() {

        ArrayList<Person> personList = new ArrayList<Person>();

        try {
            String sql
                    = "SELECT id, lastname, firstname, "
                    + "age "
                    + "FROM "
                    + "persons";

            personList
                    = (ArrayList<Person>) jdbcTemplate.query(
                            sql,
                            new BeanPropertyRowMapper(Person.class
                            )
                    );

        } catch (DataAccessException dataAccessException) {
            System.out.println("A DataAccessException ocurred in the method "
                    + "testMysql ");
            dataAccessException.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return personList;
    }

}
