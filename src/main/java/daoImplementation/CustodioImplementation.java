package daoImplementation;

import dao.CustodioBankDAO;
import exceptions.CustomException;
import java.math.BigDecimal;
import java.sql.Timestamp;
import javax.sql.DataSource;
import model.CashPetition;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;

public class CustodioImplementation implements CustodioBankDAO {

    private JdbcTemplate jdbcTemplate;

    public CustodioImplementation(DataSource datasource) {
        this.jdbcTemplate = new JdbcTemplate(datasource);
    }

    @Override
    public BigDecimal getCurrentCashAmout() {
        BigDecimal currentAmountInBankDeposited = null;
        BigDecimal currentAmountInBankWithdraw = null;
        BigDecimal currentAmountInBank = null;

        try {
            String sql
                    = "SELECT SUM (amount)"
                    + "FROM "
                    + "money_bank "
                    + "WHERE "
                    + "type = 'deposit' ";

            currentAmountInBankDeposited = (BigDecimal) jdbcTemplate.queryForObject(
                    sql,
                    BigDecimal.class
            );

            String sqlWithdraw
                    = "SELECT SUM (amount)"
                    + "FROM "
                    + "money_bank "
                    + "WHERE "
                    + "type = 'withdraw' ";

            currentAmountInBankWithdraw = (BigDecimal) jdbcTemplate.queryForObject(
                    sqlWithdraw,
                    BigDecimal.class
            );

            if (currentAmountInBankDeposited != null && currentAmountInBankWithdraw != null) {
                currentAmountInBank = currentAmountInBankDeposited.subtract(currentAmountInBankWithdraw);
            } else if (currentAmountInBankDeposited != null && currentAmountInBankWithdraw == null){
                currentAmountInBank = currentAmountInBankDeposited;
            } else if (currentAmountInBankDeposited == null && currentAmountInBankWithdraw != null){
                currentAmountInBank = currentAmountInBankWithdraw;
            } else {
                currentAmountInBank = null;
            }

        } catch (DataAccessException e) {
            System.out.println("A DataAccessException ocurred in the method "
                    + "CustodioImplementation.getCurrentCashAmout ");
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return currentAmountInBank;
    }

    @Override
    public void registeringMoneyInBank(BigDecimal amount) {
        try {
            Timestamp creationDate = new Timestamp(System.currentTimeMillis());

            String sql
                    = "INSERT INTO money_bank (amount, creation_date, type) "
                    + "VALUES(?, ?, ?)";

            jdbcTemplate.update(sql, new Object[]{amount, creationDate, "deposit"});

        } catch (DataAccessException dataAccessException) {
            System.out.println("A DataAccessException ocurred in the method "
                    + "CustodioImplementation.registeringMoneyInBank ");
            dataAccessException.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void withdrawMoneyInBank(BigDecimal amount) throws CustomException {
        try {
            Timestamp creationDate = new Timestamp(System.currentTimeMillis());
            
            if (getCurrentCashAmout() == null || isPetitionGreaterThanCurrentCash(amount) == false) {
                throw new CustomException("500", "there's not enought money "
                        + "to withdraw");
            } else {
                String sql
                        = "INSERT INTO money_bank (amount, creation_date, type) "
                        + "VALUES(?, ?, ?)";

                jdbcTemplate.update(sql, new Object[]{amount, creationDate, "withdraw"});
            }

        } catch (CustomException e) {
            e.printStackTrace();
            throw new CustomException(e.getCodeError(), e.getMessageError());
        } catch (DataAccessException dataAccessException) {
            System.out.println("A DataAccessException ocurred in the method "
                    + "CustodioImplementation.withdrawMoneyInBank ");
            dataAccessException.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean isPetitionGreaterThanCurrentCash(BigDecimal petitionAmount) {
        boolean isPetitionValid;

        if (getCurrentCashAmout().compareTo(petitionAmount) >= 0) {
            isPetitionValid = true;
        } else {
            isPetitionValid = false;
        }

        return isPetitionValid;
    }

    @Override
    public void approvePetition(CashPetition petition) throws CustomException {
        try {

            if (isPetitionGreaterThanCurrentCash(petition.getAmount()) == false) {
                throw new CustomException("500", "there's not enought money "
                        + "to withdraw");
            } else {
                withdrawMoneyInBank(petition.getAmount());

                Timestamp approvedDate = new Timestamp(System.currentTimeMillis());
                String sql = "UPDATE "
                        + "petition "
                        + "SET "
                        + "status = 'approved', "
                        + "approved_rejected_date = ? "
                        + "WHERE "
                        + "id_petition = ?";

                jdbcTemplate.update(sql, approvedDate, petition.getIdPetition());
            }

        } catch (CustomException e) {
            e.printStackTrace();
            throw new CustomException(e.getCodeError(), e.getMessageError());
        } catch (DataAccessException dataAccessException) {
            System.out.println("A DataAccessException ocurred in the method "
                    + "CustodioImplementation.approvePetition ");
            dataAccessException.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
//    @Override
//    public void modificarCategoria(Categoria categoria) throws ExcepcionCustomizada {
//
//        try {
//
//            String sql
//                    = "UPDATE "
//                    + "CATEGORIA "
//                    + "SET "
//                    + "NOMBRE  =  ?, "
//                    + "DESCRIPCION  =  ?, "
//                    + "TIPO  =  ?, "
//                    + "Estado = ? "
//                    + "WHERE "
//                    + "ID_CATEGORIA  =  ? ";
//
//            Integer idCatEditada
//                    = buscarNombreCategoriaRepetida(categoria.getNombre());
//
//            if (idCatEditada != null && idCatEditada != categoria.getIdCategoria()) {
//                throw new ExcepcionCustomizada("500", "ya existe una categoria con este nombre");
//            } else {
//                jdbcTemplate.update(sql,
//                        categoria.getNombre(), categoria.getDescripcion(),
//                        categoria.getTipo(), categoria.getEstado(),
//                        categoria.getIdCategoria()
//                );
//
//                Usuario usuario = usuarioDAO.buscarNombreIdUsuarioSesionado();
//                LogAccionesUsuario logAccionesUsuario = new LogAccionesUsuario("modificar categoria",
//                        "se modifico una categoria llamada: " + categoria.getNombre(), "categoria", usuario.getNombreUsuario(),
//                        null, usuario.getIdUsuario());
//                logAccionesDAO.insertarLogAccionUsuario(logAccionesUsuario);
//
//            }
//        } catch (ExcepcionCustomizada e) {
//            System.out.println("Se produjo una excepcion en el metodo modificarCategoria");
//            System.out.println("el error es ++ " + e.getMensajeError());
//            e.printStackTrace();
//            throw new ExcepcionCustomizada(e.getCodigoError(), e.getMensajeError());
//        } catch (DataAccessException dataAccessException) {
//
//            System.out.println("Se produjo una excepcion en el metodo modificarCategoria");
//            dataAccessException.printStackTrace();
//        } catch (Exception e) {
//
//            System.out.println("Se produjo una excepcion en el metodo modificarCategoria");
//            e.printStackTrace();
//        }
//
//    }
//
//    @Override
//    public boolean eliminarCategoriaPorId(int idCategoria) {
//        boolean resultado = false;
//        try {
//            String sql
//                    = "DELETE FROM "
//                    + "CATEGORIA  "
//                    + "WHERE "
//                    + "ID_CATEGORIA= ? ";
//
//            Categoria categoria = buscarCategoriaPorId(idCategoria);
//
//            jdbcTemplate.update(
//                    sql,
//                    new Object[]{idCategoria}
//            );
//
//            Usuario usuario = usuarioDAO.buscarNombreIdUsuarioSesionado();
//            LogAccionesUsuario logAccionesUsuario = new LogAccionesUsuario("eliminar categoria",
//                    "se elimino una categoria llamada: " + categoria.getNombre(), "categoria", usuario.getNombreUsuario(),
//                    null, usuario.getIdUsuario());
//            logAccionesDAO.insertarLogAccionUsuario(logAccionesUsuario);
//
//            resultado = true;
//        } catch (DataAccessException dataAccessException) {
//            System.out.println("Se produjo una excepcion en el metodo eliminarCategoriaPorId");
//            dataAccessException.printStackTrace();
//        } catch (Exception e) {
//            System.out.println("Se produjo una excepcion en el metodo eliminarCategoriaPorId");
//            e.printStackTrace();
//        }
//
//        return resultado;
//    }

}
