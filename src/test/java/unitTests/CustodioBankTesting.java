package unitTests;

import config.ConfigMVC;
import dao.CustodioBankDAO;
import exceptions.CustomException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.CashPetition;
import model.Person;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jca.cci.InvalidResultSetAccessException;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.AnnotationConfigWebContextLoader;
import org.springframework.test.context.web.WebAppConfiguration;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = ConfigMVC.class, loader = AnnotationConfigWebContextLoader.class)
public class CustodioBankTesting {
    
    @Autowired
    private CustodioBankDAO custodioBankDAO;
    
    
    @Test
    public void approvePetitionTest() {
        try {
            CashPetition petition = new CashPetition(1, "petitionTEST", new BigDecimal("2662.22"), null, "approved", null);
            
            custodioBankDAO.approvePetition(petition);
            
        } catch (InvalidResultSetAccessException e) {
            throw new RuntimeException(e);
        } catch (DataAccessException e) {
            throw new RuntimeException(e);
        } catch (CustomException ex) {
            Logger.getLogger(CustodioBankTesting.class.getName()).log(Level.SEVERE, null, ex);
        } 
    }
    
    @Test
    public void registerMoney() {
        try {
            BigDecimal amountToDeposit= new BigDecimal("555.55"); 
            
            custodioBankDAO.registeringMoneyInBank(amountToDeposit);
            
        } catch (InvalidResultSetAccessException e) {
            throw new RuntimeException(e);
        } catch (DataAccessException e) {
            throw new RuntimeException(e);
        } 
    }
    
    @Test
    public void withdrawMoneyTest() {
        try {
            BigDecimal amountToWithdraw= new BigDecimal("1000.55"); 
            
            custodioBankDAO.withdrawMoneyInBank(amountToWithdraw);
            
        } catch (InvalidResultSetAccessException e) {
            throw new RuntimeException(e);
        } catch (DataAccessException e) {
            throw new RuntimeException(e);
        } catch (CustomException ex) {
            Logger.getLogger(CustodioBankTesting.class.getName()).log(Level.SEVERE, null, ex);
        } 
    }
    
    @Test
    public void getCurrentCash() {
        try {
            System.out.println("current cash " + custodioBankDAO.getCurrentCashAmout());
        } catch (InvalidResultSetAccessException e) {
            throw new RuntimeException(e);
        } catch (DataAccessException e) {
            throw new RuntimeException(e);
        } 
    }
    
    
}
