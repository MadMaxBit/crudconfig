package unitTests;

import config.ConfigMVC;
import dao.CashPetitionDAO;
import exceptions.CustomException;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.CashPetition;
import model.Person;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jca.cci.InvalidResultSetAccessException;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.AnnotationConfigWebContextLoader;
import org.springframework.test.context.web.WebAppConfiguration;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = ConfigMVC.class, loader = AnnotationConfigWebContextLoader.class)
public class PetitionTesting {

    CashPetition petition;

    @Autowired
    private CashPetitionDAO petitionDAO;

    @Before
    public void setUp() {
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        petition = new CashPetition(1, "petitionTEST", new BigDecimal("22.22"), timestamp, "approved", null);
    }

    @Test
    public void insertPetitionTest() {
        try {
            petitionDAO.insertPetition(petition);
        } catch (InvalidResultSetAccessException e) {
            throw new RuntimeException(e);
        } catch (DataAccessException e) {
            throw new RuntimeException(e);
        } catch (CustomException ex) {
            Logger.getLogger(PetitionTesting.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Test
    public void getAllPetitions() {
        try {
            List<CashPetition> lista = petitionDAO.getAllPetitions();
        } catch (InvalidResultSetAccessException e) {
            throw new RuntimeException(e);
        } catch (DataAccessException e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    public void getAllPetitionsByStatus() {
        try {
            List<CashPetition> lista = petitionDAO.getAllPetitionsByStatus("pending");
            System.out.println(lista.size());
        } catch (InvalidResultSetAccessException e) {
            throw new RuntimeException(e);
        } catch (DataAccessException e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    public void testMysqlCon() {
        ArrayList<Person> personList = new ArrayList<Person>();

        personList = petitionDAO.testMysql();
        System.out.println("PERSONAS");
        for (Person item : personList) {
            System.out.println("******");
            System.out.println("Nombre: " + item.getFirstName());
            System.out.println("Apellido: " + item.getLastName());
            System.out.println("Edad: " + item.getAge());
            System.out.println("******");
        }

        System.out.println("fff");
    }

}
